<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(1);

defined('APPLICATION_PATH')
|| define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/application'));

defined('APPLICATION_ROOT')
|| define('APPLICATION_ROOT', str_replace('/index.php', '', $_SERVER['SCRIPT_NAME']));

/* load additional, global function */
require APPLICATION_PATH . '/../library/functions.php';

/* init autoloader and add some extra namespaces */
require APPLICATION_PATH . '/../library/vinyl/autoloader.php';
$autoloader = new vinyl_autoloader();
$autoloader->initAutoloader();

require APPLICATION_PATH . '/bootstrap.php';
$application = new bootstrap();