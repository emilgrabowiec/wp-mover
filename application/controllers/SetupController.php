<?php
class SetupController extends vinyl_controller {
	
	public function updateAction() {
		
		if($this->isPost()) {
			try {
				$arrData = $this->getAllParams();
				if($this->isValid($arrData)) {
					/* remove last '/' from previous domain name */
					if($arrData['prev-domain'][strlen($arrData['prev-domain'])-1] == '/')
						$arrData['prev-domain'] = substr($arrData['prev-domain'], 0, strlen($arrData['prev-domain'])-2);
					
					/* remove last '/' from target domain name */
					if($arrData['target-domain'][strlen($arrData['target-domain'])-1] == '/')
						$arrData['target-domain'] = substr($arrData['target-domain'], 0, strlen($arrData['target-domain'])-2);
					
					$this->updateDatabase($arrData);
					$this->view->updateComplete = true;
				}
			} catch(Exception $ex) {
				$this->view->isError = true;
				$this->view->msgError = $ex->getMessage();
			}
		}
	}
	
	private function isValid($arrData) {
		/* setup required fields from post */
		$reqFields = array(
			'db-name',
			'wp-prefix',
			'user-name',
			'host',
			'prev-domain',
			'target-domain',
		);
		
		foreach ($reqFields as $field) {
			if(!isset($arrData[$field]) || empty($arrData[$field])) 
				throw new Exception(sprintf('Missing data field in request: "%s"', $field));
		}
		
		if(!preg_match('/^http:\/\//', $arrData['prev-domain']) || (strlen($arrData['prev-domain']) <= 7)) 
			throw new Exception('Missing "http://" part in "Previous domain" field or value is invalid.');
		
		if(!preg_match('/^http:\/\//', $arrData['target-domain']) || (strlen($arrData['target-domain']) <= 7)) 
			throw new Exception('Missing "http://" part in "Target domain" field or value is invalid.');
		
		return true;
	}
	
	private function updateDatabase($arrData) {
		$strConnection = sprintf('mysql:host=%s;dbname=%s', $arrData['host'], $arrData['db-name']);
		$pdo = new PDO($strConnection, $arrData['user-name'], $arrData['user-password']);

		$updateOptions = sprintf("
			UPDATE %soptions SET
				option_value = replace(option_value, '%s', '%s')
			WHERE
				option_name = 'home' OR option_name = 'siteurl';
			",
			$arrData['wp-prefix'],
			$arrData['prev-domain'],
			$arrData['target-domain']
		);
		
		$updatePostsGuid = sprintf("
			UPDATE %sposts SET
				guid = replace(guid, '%s','%s');
			",
			$arrData['wp-prefix'],
			$arrData['prev-domain'],
			$arrData['target-domain']
		);
		
		$updatePostsContent = sprintf("
			UPDATE %sposts SET
				post_content = replace(post_content, '%s', '%s');
			",
			$arrData['wp-prefix'],
			$arrData['prev-domain'],
			$arrData['target-domain']
		);

		$countOptions = $pdo->exec($updateOptions);
		$countPostsGuid = $pdo->exec($updatePostsGuid);
		$countPostsContent = $pdo->exec($updatePostsContent);
		
		$this->view->countOptions 		= $countOptions;
		$this->view->countPostsGuid 	= $countPostsGuid;
		$this->view->countPostsContent 	= $countPostsContent;
	}
}