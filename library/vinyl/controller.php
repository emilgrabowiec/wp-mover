<?php

class vinyl_controller {
	protected $request;
	protected $view;
	
	public function __construct() {
		$this->view = new vinyl_view();
	}
	
	public function getView() 		{ return $this->view; }
	public function getAction() 	{ return $this->request['action']; }
	public function getController() { return $this->request['controller']; }
	
	public function setRequest($request) { $this->request = $request; }
	
	protected function getAllParams() {
		$arrParams = $this->request;
		
		unset($arrParams['controller']);
		unset($arrParams['action']);
		
		return $arrParams;
	}
	
	protected function isPost() {
		return ($_SERVER['REQUEST_METHOD'] === 'POST');
	}
}