<?php

/**
 * class should init autoloader which autoloads files with classes placed in library/ directory
 */
class vinyl_autoloader {

	public function __construct() {

	}
	
	public function initAutoloader() {
		spl_autoload_register(function ($class) {
			$classPath = implode('/', explode('_', $class));
			include APPLICATION_PATH . '/../library/' . $classPath . '.php';
		});
	}
}