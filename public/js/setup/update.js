jQuery(document).ready(function() {
	$('#submit-update').click(function() {
		$('<div>').html('Are you sure to perform WordPress database update?').dialog({
			modal: true, 
			title: 'Confirmation', 
			width: 400, 
			resizable: false,
			buttons: {
				'Yes': {
					'text': 'Yes',
					class: 'btn btn-default',
					click: function () {
						$('#frmWP').submit();
					}
				},
				Cancel: {
					'text': 'Cancel',
					class: 'btn btn-default',
					'click': function () {
						$(this).dialog("close");
					}
				}
			},
			close: function (event, ui) {
				$(this).remove();
			}
		});
		return false;
	});
});